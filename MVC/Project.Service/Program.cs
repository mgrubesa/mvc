﻿using Project.Service.DAL;
using System;

namespace Project.Service
{
    internal class Program
    {
        #region Methods

        private static void Main(string[] args)
        {
            using (var db = new VehicleContext())
            {
                db.VehicleMake.Add(new Model.VehicleMake { Name = "e", Id = 1, Abrv = "da" });
                db.SaveChanges();

                foreach (var blog in db.VehicleMake)
                {
                    Console.WriteLine(blog.Name);
                }
            }
        }

        #endregion Methods
    }
}