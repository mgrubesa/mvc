﻿using AutoMapper;
using PagedList;
using Project.Service.DAL;
using Project.Service.Interface;
using Project.Service.Model;
using Project.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Project.Service.Service
{
    public class VehicleMakeService : IVehicleMake
    {
        #region Fields

        public VehicleContext dbContext = VehicleContext.GetInstance();

        #endregion Fields

        #region Constructors

        public VehicleMakeService()
        {
        }

        #endregion Constructors

        #region Methods

        public VehicleMakeViewModel Create(VehicleMakeViewModel model)
        {
            try
            {
                model.Id = new int();
                dbContext.VehicleMake.Add(Mapper.Map<VehicleMake>(model));
                dbContext.SaveChanges();
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var toDelete = dbContext.VehicleMake.Find(id);
                dbContext.VehicleMake.Remove(toDelete);
                return dbContext.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public VehicleMakeViewModel Get(int id)
        {
            try
            {
                return Mapper.Map<VehicleMakeViewModel>(dbContext.VehicleMake.AsNoTracking().FirstOrDefault(x => x.Id == id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<VehicleMakeViewModel> GetAll()
        {
            return Mapper.Map<List<VehicleMakeViewModel>>(dbContext.VehicleMake.ToList());
        }

        public IPagedList<VehicleMakeViewModel> SortPageFilterMake(string searchBy, string search, int? page, string sortBy)
        {
            if (searchBy == "Abrv")
            {
                switch (sortBy)
                {
                    case "name_desc":
                        return Mapper.Map<IEnumerable<VehicleMakeViewModel>>(dbContext.VehicleMake.Where(x => x.Abrv.Contains(search) || search == null).OrderByDescending(s => s.Name)).ToPagedList(page ?? 1, 10);

                    case "Abrv":
                        return Mapper.Map<IEnumerable<VehicleMakeViewModel>>(dbContext.VehicleMake.Where(x => x.Abrv.Contains(search) || search == null).OrderBy(s => s.Abrv)).ToPagedList(page ?? 1, 10);

                    case "abrv_desc":
                        return Mapper.Map<IEnumerable<VehicleMakeViewModel>>(dbContext.VehicleMake.Where(x => x.Abrv.Contains(search) || search == null).OrderByDescending(s => s.Abrv)).ToPagedList(page ?? 1, 10);

                    default:
                        return Mapper.Map<IEnumerable<VehicleMakeViewModel>>(dbContext.VehicleMake.Where(x => x.Abrv.Contains(search) || search == null).OrderBy(s => s.Name)).ToPagedList(page ?? 1, 10);
                }
            }
            else
            {
                switch (sortBy)
                {
                    case "name_desc":
                        return Mapper.Map<IEnumerable<VehicleMakeViewModel>>(dbContext.VehicleMake.Where(x => x.Name.Contains(search) || search == null).OrderByDescending(s => s.Name)).ToPagedList(page ?? 1, 10);

                    case "Abrv":
                        return Mapper.Map<IEnumerable<VehicleMakeViewModel>>(dbContext.VehicleMake.Where(x => x.Name.Contains(search) || search == null).OrderBy(s => s.Abrv)).ToPagedList(page ?? 1, 10);

                    case "abrv_desc":
                        return Mapper.Map<IEnumerable<VehicleMakeViewModel>>(dbContext.VehicleMake.Where(x => x.Name.Contains(search) || search == null).OrderByDescending(s => s.Abrv)).ToPagedList(page ?? 1, 10);

                    default:
                        return Mapper.Map<IEnumerable<VehicleMakeViewModel>>(dbContext.VehicleMake.Where(x => x.Name.Contains(search) || search == null).OrderBy(s => s.Name)).ToPagedList(page ?? 1, 10);
                }
            }
        }

        public bool Update(VehicleMakeViewModel model)
        {
            dbContext.Entry(Mapper.Map<VehicleMake>(model)).State = EntityState.Modified;
            return dbContext.SaveChanges() > 0;
        }

        #endregion Methods
    }
}