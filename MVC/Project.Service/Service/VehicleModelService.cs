﻿using AutoMapper;
using PagedList;
using Project.Service.DAL;
using Project.Service.Interface;
using Project.Service.Model;
using Project.Service.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Project.Service.Service
{
    public class VehicleModelService : IVehicleModel
    {
        #region Fields

        public VehicleContext dbContext = VehicleContext.GetInstance();

        #endregion Fields

        #region Constructors

        public VehicleModelService()
        {
        }

        #endregion Constructors

        #region Methods

        public VehicleModelViewModel Create(VehicleModelViewModel model)
        {
            try
            {
                model.Id = new int();
                dbContext.VehicleModels.Add(Mapper.Map<VehicleModel>(model));
                dbContext.SaveChanges();
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var toDelete = dbContext.Set<VehicleModel>().Find(id);
                dbContext.VehicleModels.Remove(toDelete);
                return dbContext.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IPagedList<VehicleModelViewModel> Filter(string searchBy, string search, int? page, string sortBy)
        {
            if (searchBy == "Abrv")
            {
                switch (sortBy)
                {
                    case "makers_desc":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.Abrv.Contains(search) || search == null).OrderByDescending(s => s.VehicleMakes.Name)).ToPagedList(page ?? 1, 10);

                    case "Abrv":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.Abrv.Contains(search) || search == null).OrderBy(s => s.Abrv)).ToPagedList(page ?? 1, 10);

                    case "abrv_desc":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.Abrv.Contains(search) || search == null).OrderByDescending(s => s.Abrv)).ToPagedList(page ?? 1, 10);

                    case "name_desc":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.Abrv.Contains(search) || search == null).OrderByDescending(s => s.Name)).ToPagedList(page ?? 1, 10);

                    case "Name":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.Abrv.Contains(search) || search == null).OrderBy(s => s.Name)).ToPagedList(page ?? 1, 10);

                    default:
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.Abrv.Contains(search) || search == null).OrderBy(s => s.VehicleMakes.Name)).ToPagedList(page ?? 1, 10);
                }
            }
            else if (searchBy == "Name")
            {
                switch (sortBy)
                {
                    case "makers_desc":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.Name.Contains(search) || search == null).OrderByDescending(s => s.VehicleMakes.Name)).ToPagedList(page ?? 1, 10);

                    case "Abrv":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.Name.Contains(search) || search == null).OrderBy(s => s.Abrv)).ToPagedList(page ?? 1, 10);

                    case "abrv_desc":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.Name.Contains(search) || search == null).OrderByDescending(s => s.Abrv)).ToPagedList(page ?? 1, 10);

                    case "name_desc":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.Name.Contains(search) || search == null).OrderByDescending(s => s.Name)).ToPagedList(page ?? 1, 10);

                    case "Name":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.Name.Contains(search) || search == null).OrderBy(s => s.Name)).ToPagedList(page ?? 1, 10);

                    default:
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.Name.Contains(search) || search == null).OrderBy(s => s.VehicleMakes.Name)).ToPagedList(page ?? 1, 10);
                }
            }
            else
            {
                switch (sortBy)
                {
                    case "makers_desc":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.VehicleMakes.Name.Contains(search) || search == null).OrderByDescending(s => s.VehicleMakes.Name)).ToPagedList(page ?? 1, 10);

                    case "Abrv":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.VehicleMakes.Name.Contains(search) || search == null).OrderBy(s => s.Abrv)).ToPagedList(page ?? 1, 10);

                    case "abrv_desc":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.VehicleMakes.Name.Contains(search) || search == null).OrderByDescending(s => s.Abrv)).ToPagedList(page ?? 1, 10);

                    case "name_desc":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.VehicleMakes.Name.Contains(search) || search == null).OrderByDescending(s => s.Name)).ToPagedList(page ?? 1, 10);

                    case "Name":
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.VehicleMakes.Name.Contains(search) || search == null).OrderBy(s => s.Name)).ToPagedList(page ?? 1, 10);

                    default:
                        return Mapper.Map<IEnumerable<VehicleModelViewModel>>(dbContext.VehicleModels.Where(x => x.VehicleMakes.Name.Contains(search) || search == null).OrderBy(s => s.VehicleMakes.Name)).ToPagedList(page ?? 1, 10);
                }
            }
        }

        public VehicleModelViewModel Get(int id)
        {
            try
            {
                return Mapper.Map<VehicleModelViewModel>(dbContext.VehicleModels.AsNoTracking().FirstOrDefault(x => x.Id == id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<VehicleModelViewModel> GetAll()
        {
            return Mapper.Map<List<VehicleModelViewModel>>(dbContext.VehicleModels.ToList());
        }

        public bool Update(VehicleModelViewModel model)
        {
            dbContext.Entry(Mapper.Map<VehicleModel>(model)).State = EntityState.Modified;
            return dbContext.SaveChanges() > 0;
        }

        #endregion Methods
    }
}