﻿using System.Collections.Generic;

namespace Project.Service.Model
{
    public class VehicleMake
    {
        #region Properties

        public string Abrv { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<VehicleModel> VehicleModel { get; set; }

        #endregion Properties
    }
}