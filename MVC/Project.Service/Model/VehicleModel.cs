﻿namespace Project.Service.Model
{
    public class VehicleModel
    {
        #region Properties

        public string Abrv { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int VehicleMakeId { get; set; }
        public virtual VehicleMake VehicleMakes { get; set; }

        #endregion Properties
    }
}