﻿using Project.Service.Model;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Project.Service.DAL
{
    public class VehicleContext : DbContext
    {
        #region Fields

        private static VehicleContext Instance;

        #endregion Fields

        #region Constructors

        public VehicleContext() : base("Veh")
        {
            Database.SetInitializer<VehicleContext>(new CreateDatabaseIfNotExists<VehicleContext>());
        }

        #endregion Constructors

        #region Properties

        public DbSet<VehicleMake> VehicleMake { get; set; }

        public DbSet<VehicleModel> VehicleModels { get; set; }

        #endregion Properties

        #region Methods

        public static VehicleContext GetInstance()
        {
            if (Instance == null)
                Instance = new VehicleContext();
            return Instance;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        #endregion Methods
    }
}