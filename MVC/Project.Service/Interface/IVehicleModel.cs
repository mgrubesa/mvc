﻿using PagedList;
using Project.Service.ViewModels;
using System.Collections.Generic;

namespace Project.Service.Interface
{
    public interface IVehicleModel
    {
        #region Methods

        VehicleModelViewModel Create(VehicleModelViewModel model);

        bool Delete(int id);

        IPagedList<VehicleModelViewModel> Filter(string searchBy, string search, int? page, string sortBy);

        VehicleModelViewModel Get(int id);

        IEnumerable<VehicleModelViewModel> GetAll();

        bool Update(VehicleModelViewModel model);

        #endregion Methods
    }
}