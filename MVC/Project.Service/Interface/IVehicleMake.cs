﻿using PagedList;
using Project.Service.ViewModels;
using System.Collections.Generic;

namespace Project.Service.Interface
{
    public interface IVehicleMake
    {
        #region Methods

        VehicleMakeViewModel Create(VehicleMakeViewModel model);

        bool Delete(int id);

        //IPagedList<VehicleMakeViewModel> Filter(string searchBy, string search, int? page, string sortBy);

        VehicleMakeViewModel Get(int id);

        IEnumerable<VehicleMakeViewModel> GetAll();

        IPagedList<VehicleMakeViewModel> SortPageFilterMake(string searchBy, string search, int? page, string sortBy);

        bool Update(VehicleMakeViewModel model);

        #endregion Methods
    }
}