﻿using Project.Service.Model;
using System.Collections.Generic;

namespace Project.Service.ViewModels
{
    public class VehicleMakeViewModel
    {
        #region Properties

        public string Abrv { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<VehicleModel> VehicleModel { get; set; }

        #endregion Properties
    }
}