﻿using Project.Service.Model;

namespace Project.Service.ViewModels
{
    public class VehicleModelViewModel
    {
        #region Properties

        public string Abrv { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int VehicleMakeId { get; set; }
        public virtual VehicleMake VehicleMakes { get; set; }

        #endregion Properties
    }
}