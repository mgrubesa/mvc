﻿using Project.Service.Service;
using Project.Service.ViewModels;
using System;
using System.Net;
using System.Web.Mvc;

namespace WebApplication6.Controllers
{
    public class VehicleMakesController : Controller
    {
        #region Fields

        public VehicleMakeService Service;

        #endregion Fields

        #region Constructors

        public VehicleMakesController()
        {
            Service = new VehicleMakeService();
        }

        #endregion Constructors

        #region Methods

        // GET: VehicleMakes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VehicleMakes/Create To protect from overposting attacks, please enable the specific properties you want
        // to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(VehicleMakeViewModel vehicleMake)
        {
            if (ModelState.IsValid)
            {
                Service.Create(vehicleMake);

                return RedirectToAction("Index");
            }

            return View(vehicleMake);
        }

        // GET: VehicleMakes/Delete/5
        public ActionResult Delete(int id)
        {
            Service.Delete(id);
            return RedirectToAction("Index");
        }

        // POST: VehicleMakes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            return RedirectToAction("Index");
        }

        // GET: VehicleMakes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ;

            return View();
        }

        // GET: VehicleMakes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View();
        }

        // POST: VehicleMakes/Edit/5 To protect from overposting attacks, please enable the specific properties you want
        // to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Abrv,Name")] VehicleMakeViewModel vehicleMake)
        {
            if (ModelState.IsValid)
            {
                Service.Update(vehicleMake);
            }
            return View(vehicleMake);
        }

        // GET: VehicleMakes
        public ActionResult Index(string searchBy, string search, int? page, string sortBy)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortBy) ? "name_desc" : "";
            ViewBag.AbrvSortParm = sortBy == "Abrv" ? "abrv_desc" : "Abrv";

            return View(Service.SortPageFilterMake(searchBy, search, page, sortBy));
        }

        #endregion Methods
    }
}