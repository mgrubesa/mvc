﻿using Project.Service.Service;
using Project.Service.ViewModels;
using System;
using System.Net;
using System.Web.Mvc;

namespace WebApplication6.Controllers
{
    public class VehicleModelsController : Controller
    {
        #region Fields

        private VehicleModelService Service;

        #endregion Fields

        #region Constructors

        public VehicleModelsController()
        {
            Service = new VehicleModelService();
        }

        #endregion Constructors

        #region Methods

        // GET: VehicleModels/Create
        public ActionResult Create()
        {
            ViewBag.VehicleMakeId = new SelectList(Service.GetAll(), "Id", "Name");
            return View();
        }

        // POST: VehicleModels/Create To protect from overposting attacks, please enable the specific properties you want
        // to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Abrv,Name,VehicleMakeId")] VehicleModelViewModel vehicleModel)
        {
            if (ModelState.IsValid)
            {
                Service.Create(vehicleModel);
                return RedirectToAction("Index");
            }
            ViewBag.VehicleMakeId = new SelectList(Service.GetAll(), "Id", "Name", vehicleModel.VehicleMakeId);

            return View(vehicleModel);
        }

        // GET: VehicleModels/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleModelViewModel vehicleModel = Service.Get(id);
            if (vehicleModel == null)
            {
                return HttpNotFound();
            }

            return View(vehicleModel);
        }

        // POST: VehicleModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Service.Delete(id);
            return RedirectToAction("Index");
        }

        // GET: VehicleModels/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleModelViewModel vehicleModel = Service.Get(id);
            if (vehicleModel == null)
            {
                return HttpNotFound();
            }
            return View(vehicleModel);
        }

        // GET: VehicleModels/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VehicleModelViewModel vehicleModel = Service.Get(id);
            if (vehicleModel == null)
            {
                return HttpNotFound();
            }

            ViewBag.VehicleMakeId = new SelectList(Service.GetAll(), "Id", "Name", vehicleModel.VehicleMakeId);

            return View(vehicleModel);
        }

        // POST: VehicleModels/Edit/5 To protect from overposting attacks, please enable the specific properties you want
        // to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Abrv,Name,VehicleMakeId")] VehicleModelViewModel vehicleModel)
        {
            if (ModelState.IsValid)
            {
                Service.Update(vehicleModel);
            }
            ViewBag.VehicleMakeId = new SelectList(Service.GetAll(), "Id", "Name", vehicleModel.VehicleMakeId);

            return View(vehicleModel);
        }

        // GET: VehicleModels
        public ActionResult Index(string searchBy, string search, int? page, string sortBy)
        {
            ViewBag.MakersNameSortParm = String.IsNullOrEmpty(sortBy) ? "makers_desc" : "";
            ViewBag.AbrvSortParm = sortBy == "Abrv" ? "abrv_desc" : "Abrv";
            ViewBag.NameSortParm = sortBy == "Name" ? "name_desc" : "Name";

            return View(Service.Filter(searchBy, search, page, sortBy));
        }

        protected override void Dispose(bool disposing)
        {
        }

        #endregion Methods
    }
}