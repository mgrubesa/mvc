﻿using AutoMapper;
using Project.Service.Model;
using Project.Service.ViewModels;

namespace WebApplication6.App_Start
{
    public static class Maping
    {
        #region Methods

        public static void RegisterMaps()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<VehicleMake, VehicleMakeViewModel>().ReverseMap();
                config.CreateMap<VehicleModel, VehicleModelViewModel>().ReverseMap();
            });
        }

        #endregion Methods
    }
}