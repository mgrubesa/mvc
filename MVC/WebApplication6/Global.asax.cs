﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebApplication6.App_Start;

namespace WebApplication6
{
    public class MvcApplication : System.Web.HttpApplication
    {
        #region Methods

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Maping.RegisterMaps();
        }

        #endregion Methods
    }
}